<?php

namespace App\Http\Controllers;

use App\Biodata;
use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function index(){
        $biodata = auth()->user()->biodata;
        
        return response()->json([
            'success'=>true,
            'data'=>$biodata
        ]);
    }

    public function showBiodata($id_biodata){
        $biodata = auth()->user()->biodata()->find($id_biodata);

        if (!$biodata) {
            return response()->json([
                'success'=>true,
                'data'=>$biodata->toArray()
            ],400);
        }
    }

    public function createBiodata(Request $request){
        $this->validate($request,[
            'nama'=>'required',
            'klass'=>'required',
            'tanggal_lahir'=>'required',
            'alamat'=>'required'
        ]);

        $biodata = new Biodata();
        $biodata ->nama=$request->nama;
        $biodata ->klass=$request->klass;
        $biodata ->tanggal_lahir=$request->tanggal_lahir;
        $biodata ->alamat=$request->alamat;


        if (auth()->user()->products()->save($product))
            return response()->json([
                'success' => true,
                'data' => $product->toArray()
        ]);
        else
            return response()->json([
            'success' => false,
            'message' => 'Product could not be added'
        ], 500);
    }

    public function updateBiodata(Request $request, $id_biodata){
        $biodata = auth()->user()->biodata()->find($id_biodata);
 
        if (!$biodata) {
            return response()->json([
                'success' => false,
                'message' => 'biodata with id ' . $id_biodata . ' not found'
            ], 400);
        }
 
        $updated = $biodata->fill($request->all())->save();
 
        if ($biodata)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'biodata could not be updated'
            ], 500);
    }

    public function deleteBiodata($id_biodata){
        $biodata = auth()->user()->biodata()->find($id_biodata);
 
        if (!$biodata) {
            return response()->json([
                'success' => false,
                'message' => 'biodata with id ' . $id_biodata . ' not found'
            ], 400);
        }
 
        if ($biodata->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'biodata could not be deleted'
            ], 500);
        }
    }
}
